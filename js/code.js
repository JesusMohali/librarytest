// 1 es papel, 2 es piedra y 3 es tijera

var p1_selected
var p2_selected
var select
var player_1
var player_2
var wins = 0
var loses = 0
document.getElementById("wins").innerHTML = wins
document.getElementById("loses").innerHTML = loses
const options = ["✋", "✊", "✌"]
//Con esto se oculta las imagenes que están en el div con ID show
//mode 1 es multijugador, mode = 2 es contra la PC
if (mode == 1) {
    player_1 = "JUGADOR 1"
    player_2 = "JUGADOR 2"
    document.getElementById("show").classList.add("disable")
} else {
    player_1 = "JUGADOR"
    player_2 = "PC"
    document.getElementById("playerpc").innerHTML = "PC"
    document.getElementById("multiplayer").classList.add("disable")
    document.getElementById("multiplayer1").classList.add("disable")
}
//Nombres en el score
document.getElementById("p1_name").innerHTML = player_1
document.getElementById("p2_name").innerHTML = player_2

function reload_score() {
    document.getElementById("wins").innerHTML = wins
    document.getElementById("loses").innerHTML = loses
    if (wins == 3 || loses == 3) {
        alert("Ganaste " + wins + " y perdiste " + loses)
        if (wins > loses) {
            alert("GANÓ EL " + player_1)
        } else {
            alert("GANÓ EL " + player_2)
        }
        wins = 0
        loses = 0
        document.getElementById("wins").innerHTML = wins
        document.getElementById("loses").innerHTML = loses
    }
}
function select_option(opt) {
    select = opt
    document.getElementById("p1").innerHTML = options[opt - 1]

    if (opt == 3) {
        document.getElementById("button_1").classList.add("active")
        document.getElementById("button_2").classList.remove("active")
        document.getElementById("button_3").classList.remove("active")

    } else if (opt == 1) {
        document.getElementById("button_2").classList.add("active")
        document.getElementById("button_1").classList.remove("active")
        document.getElementById("button_3").classList.remove("active")
    }
    else if (opt == 2) {
        document.getElementById("button_3").classList.add("active")
        document.getElementById("button_1").classList.remove("active")
        document.getElementById("button_2").classList.remove("active")
    }
    random_select()
}

function random_select() {
    // n = Math.floor(Math.random()*(max - min + 1) + min)   Para que el número sea aleatorio por esos rangos
    let min = 1
    let max = 3
    let selection = document.getElementById("app")

    n = Math.floor(Math.random() * (max - min + 1) + min)
    document.getElementById("p2").innerHTML = options[n - 1]

    console.log(n)

    if (select == n) {
        selection.innerHTML = "EMPATE"
    } else if (select == 1 && n == 2 || select == 2 && n == 3 || select == 3 && n == 1) {
        selection.innerHTML = "GANASTE"
        wins = wins + 1
    } else {
        selection.innerHTML = "PERDISTE"
        loses = loses + 1
    }
    document.getElementById("playerselections").classList.remove("disable")
    reload_score()

}

//Función creada para las selecciones de los jugadores
function player_selection(num, p) {

    console.log(p)
    if (p == 1) {
        p1_selected = num
        document.getElementById("p1").innerHTML = options[num - 1]
        document.getElementById("player").innerHTML = 2
        document.getElementById("player1").classList.add("disable")
        document.getElementById("player2").classList.remove("disable")
    } else if (p == 2) {
        result(num)
    }

}

function result(num) {
    p2_selected = num
    document.getElementById("p2").innerHTML = options[num - 1]
    document.getElementById("multiplayer1").classList.add("disable")
    document.getElementById("playerselections").classList.remove("disable")
    if (p1_selected == p2_selected) {
        document.getElementById("multiplayer").innerHTML = "EMPATE"
        document.getElementById("finish").classList.remove("disable")

    } else if (p1_selected == 1 && p2_selected == 2 || p1_selected == 2 && p2_selected == 3 || p1_selected == 3 && p2_selected == 1) {
        document.getElementById("multiplayer").innerHTML = "GANO EL JUGADOR 1"
        document.getElementById("finish").classList.remove("disable")
        wins = wins + 1
    } else {
        document.getElementById("multiplayer").innerHTML = "GANO EL JUGADOR 2"
        document.getElementById("finish").classList.remove("disable")
        loses = loses + 1
    }
    reload_score()
}

//Reinicia el juego para el multijugador
function restart() {
    document.getElementById("multiplayer").innerHTML = "Jugador <span id='player'>1</span>, es tu turno"
    document.getElementById("multiplayer1").classList.remove("disable")
    document.getElementById("playerselections").classList.add("disable")
    document.getElementById("finish").classList.add("disable")
    document.getElementById("player1").classList.remove("disable")
    document.getElementById("player2").classList.add("disable")
}